# betsport

- Install Docker
- Go to sportsbet/db_dumps and copy the latest dump.sql to ./dump/dump.sql
- Run on terminal "docker-compose build"
- Then "docker-compose up"
- Access localhost:9000 for the website, localhost:8000 for phpmyadmin
- All done. Each 15min it will get new matches and odds and each hour it will generate predictions

## Credentials

Check docker-compose.yml for credentials

### Copyright

Fábio Albuquerque, Nuno Serrador, Jorge Pereira, Tiago Fernandes

#### TODO

- [x] API Authentication on delete/create/update routes - JWT
- [x] After generating the odds, insert only the last if there is already generated odds for the match
- [x] Clean up chrome processes after getting bets, sometimes it keep crashing after too many are open on bg
- [x] Check the problem with duplicate matches on the scrapper, maybe due to timezone
- [ ] Get everything in kubernetes with Gunicorn and Nginx for the REST API
- [ ] Develop mobile app with React Native
- [ ] Delete call on db on scrapper and ML and add calls to api
- [ ] Send all predicted odds in a single payload / json
