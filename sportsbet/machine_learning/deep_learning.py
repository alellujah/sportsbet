import h2o
from h2o.automl import H2OAutoML
from h2o.estimators import H2ODeepLearningEstimator
import os
import requests
import math
from techinal_analysis import doTA
from os.path import isfile, join
import re


def checkIfThereIsGenOdds(apiURL, matchID):
    print('i got here ', matchID)
    urlToCheckIfThereIsGenOdds = 'http://{}:5000/match/{}/oddPredictions'.format(
        apiURL, matchID)
    response = requests.get(urlToCheckIfThereIsGenOdds)
    print(response.status_code)
    if response.status_code != 404:
        return True
    else:
        return False


def getAccessToken(url):
    tokenUrl = 'http://{}:5000/token'.format(url)
    response = requests.post(tokenUrl, json={
                             'username': 'scrapper', 'password': '5a49833d1d6c6aafa8e4b68a7f83d613dc2bfeafd5ab88996a36baeb573e0543'})
    response_Json = response.json()
    return response_Json['access_token']


def generatePredictions():
    # do technical analysis
    doTA()

    if "API_URL" in os.environ:
        apiURL = os.environ['API_URL']
    else:
        apiURL = 'localhost'

    token = getAccessToken(apiURL)

    headers = {"Authorization": "Bearer {}".format(token)}
    url = 'http://{}:5000/insertOddPredictions'.format(apiURL)
    # urlToDelete = 'http://{}:5000/deleteOddPredictions/'.format(apiURL)

    h2o.init()

    listOfMatches = [f for f in os.listdir(
        './csv/generated/matchesTA') if isfile(join('./csv/generated/matchesTA', f))]
    for word in listOfMatches[:]:
        if word.startswith('.'):
            listOfMatches.remove(word)  # delete hidden files
    for match in listOfMatches:
        #     # fazer delete a previsões antigas
        #     deleteReq = requests.delete(
        #         urlToDelete + re.findall("\d+", match)[0], headers=headers)
        #     print(deleteReq.text)

        print("Working on {}...".format(match))
        csv = "./csv/generated/matchesTA/{}".format(match)
        csvToSave = "./csv/generated/matchesPredictions/PREDICTION-{}".format(
            match[3:])

        # Read odds with technical analysis dataframe
        matches = h2o.import_file(csv)

        matches["oddType"] = matches["oddType"].asfactor()
        matches["open"] = matches["open"].asnumeric()
        matches["high"] = matches["high"].asnumeric()
        matches["low"] = matches["low"].asnumeric()
        matches["close"] = matches["close"].asnumeric()
        matches["volume"] = matches["volume"].asnumeric()
        matches["MACD_12_26_9"] = matches["MACD_12_26_9"].asnumeric()
        matches["MACDH_12_26_9"] = matches["MACDH_12_26_9"].asnumeric()
        matches["MACDS_12_26_9"] = matches["MACDS_12_26_9"].asnumeric()
        matches["SMA_10"] = matches["SMA_10"].asnumeric()
        matches["BBL_5"] = matches["BBL_5"].asnumeric()
        matches["BBM_5"] = matches["BBM_5"].asnumeric()
        matches["BBU_5"] = matches["BBU_5"].asnumeric()
        matches["HMA_10"] = matches["HMA_10"].asnumeric()
        matches["OBV"] = matches["OBV"].asnumeric()
        matches["AROOND_14"] = matches["AROOND_14"].asnumeric()
        matches["AROONU_14"] = matches["AROONU_14"].asnumeric()
        matches["AROONOSC_14"] = matches["AROONOSC_14"].asnumeric()

        # Build and train the model:
        model = H2ODeepLearningEstimator(distribution="tweedie",
                                         hidden=[1],
                                         epochs=1000,
                                         train_samples_per_iteration=-1,
                                         reproducible=True,
                                         activation="Tanh",
                                         single_node_mode=False,
                                         balance_classes=False,
                                         force_load_balance=False,
                                         seed=23123,
                                         tweedie_power=1.5,
                                         score_training_samples=0,
                                         score_validation_samples=0,
                                         stopping_rounds=0)

        model.train(y="close", training_frame=matches)
        predict = model.predict(matches)

        # Eval performance:
        perf = model.model_performance()
        print(perf)

        # merge predictions do dataset
        fullDataSet = matches.cbind(predict)

        # class for object
        class PredictionOdd:
            def __init__(self,
                         oddID,
                         matchID,
                         oddType,
                         openOdd,
                         highestOdd,
                         lowestOdd,
                         actualOdd,
                         volumeOdd,
                         MACD_12_26_9,
                         MACDH_12_26_9,
                         MACDS_12_26_9,
                         SMA_10,
                         BBL_5,
                         BBM_5,
                         BBU_5,
                         HMA_10,
                         OBV,
                         AROOND_14,
                         AROONU_14,
                         AROONOSC_14,
                         prediction):
                self.oddID = oddID
                self.matchID = matchID
                self.oddType = oddType
                self.openOdd = openOdd
                self.highestOdd = highestOdd
                self.lowestOdd = lowestOdd
                self.actualOdd = actualOdd
                self.volumeOdd = volumeOdd
                self.MACD_12_26_9 = MACD_12_26_9
                self.MACDH_12_26_9 = MACDH_12_26_9
                self.MACDS_12_26_9 = MACDS_12_26_9
                self.SMA_10 = SMA_10
                self.BBL_5 = BBL_5
                self.BBM_5 = BBM_5
                self.BBU_5 = BBU_5
                self.HMA_10 = HMA_10
                self.OBV = OBV
                self.AROOND_14 = AROOND_14
                self.AROONU_14 = AROONU_14
                self.AROONOSC_14 = AROONOSC_14
                self.prediction = prediction

            def asdict(self):
                return {
                    'oddID': self.oddID,
                    'matchID': self.matchID,
                    'oddType': self.oddType,
                    'openOdd': self.openOdd,
                    'highestOdd': self.highestOdd,
                    'lowestOdd': self.lowestOdd,
                    'actualOdd': self.actualOdd,
                    'volumeOdd': self.volumeOdd,
                    'MACD_12_26_9': self.MACD_12_26_9 if not math.isnan(self.MACD_12_26_9) else None,
                    'MACDH_12_26_9': self.MACDH_12_26_9 if not math.isnan(self.MACDH_12_26_9) else None,
                    'MACDS_12_26_9': self.MACDS_12_26_9 if not math.isnan(self.MACDS_12_26_9) else None,
                    'SMA_10': self.SMA_10 if not math.isnan(self.SMA_10) else None,
                    'BBL_5': self.BBL_5 if not math.isnan(self.BBL_5) else None,
                    'BBM_5': self.BBM_5 if not math.isnan(self.BBM_5) else None,
                    'BBU_5': self.BBU_5 if not math.isnan(self.BBU_5) else None,
                    'HMA_10': self.HMA_10 if not math.isnan(self.HMA_10) else None,
                    'OBV': self.OBV if not math.isnan(self.OBV) else None,
                    'AROOND_14': self.AROOND_14 if not math.isnan(self.AROOND_14) else None,
                    'AROONU_14': self.AROONU_14 if not math.isnan(self.AROONU_14) else None,
                    'AROONOSC_14': self.AROONOSC_14 if not math.isnan(self.AROONOSC_14) else None,
                    'predict': self.prediction if not math.isnan(self.prediction) else None,
                }

        # convert to panda df to be able to iterate and build objects to send to api
        pd_frame = fullDataSet.as_data_frame(use_pandas=True)
        prediction_odds = []
        for index, row in pd_frame.iterrows():
            odd = PredictionOdd(row["oddID"],
                                row["matchID"],
                                row["oddType"],
                                row["open"],
                                row["high"],
                                row["low"],
                                row["close"],
                                row["volume"],
                                row["MACD_12_26_9"],
                                row["MACDH_12_26_9"],
                                row["MACDS_12_26_9"],
                                row["SMA_10"],
                                row["BBL_5"],
                                row["BBM_5"],
                                row["BBU_5"],
                                row["HMA_10"],
                                row["OBV"],
                                row["AROOND_14"],
                                row["AROONU_14"],
                                row["AROONOSC_14"],
                                row["predict"],
                                )
            prediction_odds.append(odd)
        # envio para a api
        if (checkIfThereIsGenOdds(apiURL, prediction_odds[-1].matchID)):
            # se ja existir so coloca a ultima gerada
            post = requests.post(
                url, data=prediction_odds[-1].asdict(), headers=headers)
            print(post.text)
        else:
            # se não cria as odds todas
            for odd in prediction_odds:
                post = requests.post(url, data=odd.asdict(), headers=headers)
                print(post.text)

        h2o.export_file(
            fullDataSet,
            csvToSave
        )

        print('Predictions generated for {}'.format(match))


generatePredictions()
