import pandas as pd
import pandas_ta as ta
import os
import mysql.connector
from mysql.connector import Error
from os.path import isfile, join
import glob


def generateNewCSV():

    if "DB" in os.environ:
        dbHost = os.environ['DB']
    else:
        dbHost = 'localhost'

    HOST = dbHost
    DATABASE = 'sportsbet'
    USER = 'root'
    PASSWORD = 'ola'

    try:
        connection = mysql.connector.connect(
            host=HOST,
            database=DATABASE,
            user=USER,
            password=PASSWORD)
        cursor = connection.cursor()

        mysql_getOpenMatchIDs = """SELECT * FROM flask_matches
                                WHERE matchClosed = 0"""
        # mysql_getOpenMatchIDs = """SELECT * FROM flask_matches"""
        cursor = connection.cursor()
        cursor.execute(mysql_getOpenMatchIDs)
        result = cursor.fetchall()

        # Clean if there is data
        files = glob.glob('./csv/generated/matches/*')
        for f in files:
            os.remove(f)
            print("Deleting all the matches files")
        else:
            print("Folder is empty!")

        filesTA = glob.glob('./csv/generated/matchesTA/*')
        for f in filesTA:
            os.remove(f)
            print("Deleting all the TA files")
        else:
            print("Folder is empty!")

        filesPRED = glob.glob('./csv/generated/matchesPredictions/*')
        for f in filesPRED:
            os.remove(f)
            print("Deleting all the prediction files")
        else:
            print("Folder is empty!")

        matchIds = []
        if result != None:
            for match in result:
                matchIds.append(match[0])
        else:
            print('No open matches')
            return

        for matchId in matchIds:
            cursor.callproc('getAwayOddsByMatch', [matchId])  # call procedure
            for result in cursor.stored_results():
                rows = result.fetchall()

            csv_dataset = pd.DataFrame.from_records(rows, columns=[
                "oddID", "matchID", "date", "volume", "close", "oddType", "low", "high", "open"])
            csv_dataset.to_csv(
                './csv/generated/matches/A_matchID-{}.csv'.format(matchId))

        for matchId in matchIds:
            cursor.callproc('getHomeOddsByMatch', [matchId])  # call procedure
            for result in cursor.stored_results():
                rows = result.fetchall()

            csv_dataset = pd.DataFrame.from_records(rows, columns=[
                "oddID", "matchID", "date", "volume", "close", "oddType", "low", "high", "open"])
            csv_dataset.to_csv(
                './csv/generated/matches/H_matchID-{}.csv'.format(matchId))

        for matchId in matchIds:
            cursor.callproc('getDrawOddsByMatch', [matchId])  # call procedure
            for result in cursor.stored_results():
                rows = result.fetchall()

            csv_dataset = pd.DataFrame.from_records(rows, columns=[
                "oddID", "matchID", "date", "volume", "close", "oddType", "low", "high", "open"])
            csv_dataset.to_csv(
                './csv/generated/matches/D_matchID-{}.csv'.format(matchId))

    except mysql.connector.Error as error:
        print("Failed to execute stored procedure: {}".format(error))
    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("Got new data from DB")


def doTA(file=''):
    # Get new data
    generateNewCSV()
    listOfMatches = [f for f in os.listdir(
        './csv/generated/matches') if isfile(join('./csv/generated/matches', f))]
    for word in listOfMatches[:]:
        if word.startswith('.'):
            listOfMatches.remove(word)  # delete hidden files
    for match in listOfMatches:
        csv = "./csv/generated/matches/{}".format(match)
        csvToSave = "./csv/generated/matchesTA/TA-{}".format(match[2:])

        # Load data
        df = pd.read_csv(csv, sep=',')

        # Drop column with pd auto generated id
        df.drop(df.columns[0], axis=1, inplace=True)
        print('Generating TA for {}'.format(match))
        if df.shape[0] <= 25:
            print('Not enough data to generate TA for {}'.format(match))
        else:
            try:
                df.ta.macd(cumulative=True, append=True)
                df.ta.sma(cumulative=True, append=True)
                df.ta.bbands(cumulative=True, append=True)
                df.ta.hma(cumulative=True, append=True)
                df.ta.obv(cumulative=True, append=True)
                df.ta.aroon(cumulative=True, append=True)
                # Save to csv
                if os.path.exists("./csv/generated/matchesTA/TA-{}".format(match[2:])):
                    print('File exists, append')
                    df.to_csv(csvToSave, mode='a', index=False, header=False)
                else:
                    print('File doesnt exists, create')
                    df.to_csv(csvToSave, index=False)
                print('Generated new TA for {}'.format(match[2:]))
            except Error:
                print('Error ocorred: {}'.format(Error))
                return


doTA()
