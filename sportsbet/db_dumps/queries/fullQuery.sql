(SELECT 
oddID,matchID,volume, capturedAt as 'date','away' as oddType,
(SELECT 
awayOdd
from `flask_realOdds`
WHERE matchID  = ro.matchID
ORDER BY oddID DESC
LIMIT 1) as 'close',
(SELECT 
MAX(awayOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'high',
(SELECT 
MIN(awayOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'low',
(SELECT 
awayOdd
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID
ORDER BY capturedAt ASC) as 'open'
from `flask_realOdds` as ro
GROUP BY matchID  
ORDER BY `ro`.`matchID` ASC)
UNION
(SELECT 
oddID,matchID,volume, capturedAt as 'date','draw' as oddType,
(SELECT 
drawOdd
from `flask_realOdds`
WHERE matchID  = ro.matchID
ORDER BY oddID DESC
LIMIT 1) as 'close',
(SELECT 
MAX(drawOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'high',
(SELECT 
MIN(drawOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'low',
(SELECT 
drawOdd
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID
ORDER BY capturedAt ASC) as 'open'
from `flask_realOdds` as ro
GROUP BY matchID  
ORDER BY `ro`.`matchID` ASC)
UNION
(SELECT 
oddID,matchID,volume, capturedAt as 'date','home' as oddType,
(SELECT 
homeOdd
from `flask_realOdds`
WHERE matchID  = ro.matchID
ORDER BY oddID DESC
LIMIT 1) as 'close',
(SELECT 
MAX(homeOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'high',
(SELECT 
MIN(homeOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'low',
(SELECT 
homeOdd
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID
ORDER BY capturedAt ASC) as 'open'
from `flask_realOdds` as ro
GROUP BY matchID  
ORDER BY `ro`.`matchID` ASC
)