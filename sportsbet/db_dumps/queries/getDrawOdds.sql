SELECT 
oddID,matchID,volume, capturedAt as 'date','draw' as oddType,
(SELECT 
drawOdd
from `flask_realOdds`
WHERE matchID  = ro.matchID
ORDER BY oddID DESC
LIMIT 1) as 'close',
(SELECT 
MAX(drawOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'high',
(SELECT 
MIN(drawOdd)
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID) as 'low',
(SELECT 
drawOdd
from `flask_realOdds`
WHERE matchID = ro.matchID
GROUP BY matchID
ORDER BY capturedAt ASC) as 'open'
from `flask_realOdds` as ro
GROUP BY matchID  
ORDER BY `ro`.`matchID` ASC
