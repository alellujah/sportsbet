# FLASK API

## Installation

**Installation via `requirements.txt` (to run outside of Docker)**:

```shell
$ git clone git@bitbucket.org:alellujah/sportsbet.git
$ cd sportsbet/api
$ python3 -m venv myenv
$ source myenv/bin/activate
$ pip3 install -r requirements.txt
$ flask run
```

**Installation via [Pipenv](https://pipenv-fork.readthedocs.io/en/latest/) (to run outside of Docker)**:

```shell
$ git clone git@bitbucket.org:alellujah/sportsbet.git
$ cd sportsbet/api
$ pipenv shell
$ pipenv update
$ flask run
```

## To create new kubernete images

```shell
$ eval $(minikube docker-env)
$ docker build -f Dockerfile -t sportsbet:v1.3 .
```

## To deploy to cluster

```shell
$ kubectl apply -k ./
```

## To get an external ip (when using MiniKube)

```shell
$ minikube tunnel
```

## Usage

Replace the values in **.env.example** with your values and rename this file to **.env**:

- `FLASK_APP`: Entry point of your application (should be `wsgi.py`).
- `FLASK_ENV`: The environment to run your app in (either `development` or `production`).
- `SECRET_KEY`: Randomly generated string of characters used to encrypt your app's data.
- `SQLALCHEMY_DATABASE_URI`: SQLAlchemy connection URI to a SQL database.

_Remember never to commit secrets saved in .env files to Github._

---
