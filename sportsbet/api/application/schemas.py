from .models import Match, RealOdd, PredictedOdd
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

class MatchSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Match     
        load_instance = True 
        include_fk = True

class OddSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = RealOdd     
        load_instance = True 
        include_fk = True

class PredictedOddSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = PredictedOdd
        load_instance = True 
        include_fk = True