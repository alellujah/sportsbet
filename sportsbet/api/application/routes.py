"""Application routes."""
from datetime import datetime as dt
from flask import request, render_template, make_response, redirect, url_for, request
from flask import current_app as app
from .models import db, Match, RealOdd, PredictedOdd, User
from .schemas import MatchSchema, OddSchema, PredictedOddSchema
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
# from ...machine_learning.deep_learning import generatePredictions

matches_schema = MatchSchema(many=True)
match_schema = MatchSchema()
odds_schema = OddSchema(many=True)
odd_schema = OddSchema()
predicted_odds_schema = PredictedOddSchema(many=True)
predicted_odd_schema = PredictedOddSchema()


@app.route('/refreshToken', methods=['POST'])
@jwt_refresh_token_required
def refreshToken(self):
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    return {'access_token': access_token}


@app.route('/token', methods=['POST'])
def getToken():
    content = request.json
    username = content["username"]
    password = content["password"]
    user = User.query.filter_by(username=username).first()
    if user is not None:
        if user.password == password:
            access_token = create_access_token(identity=username)
            refresh_token = create_refresh_token(identity=username)
            return {
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {"Response": "Invalid username or password"}
    else:
        return {"Response": "Invalid username or password"}


@app.route('/match', methods=['POST'])
@jwt_required
def newMatch():
    """Create a match via query string parameters."""
    homeTeam = request.args.get('homeTeam') or request.form.get('homeTeam')
    awayTeam = request.args.get('awayTeam') or request.form.get('awayTeam')
    date = request.args.get('date') or request.form.get('date')
    new_match = Match(
        homeTeam=homeTeam,
        awayTeam=awayTeam,
        date=date,
    )  # Create an instance of the Match class
    db.session.add(new_match)  # Adds new Match record to database
    db.session.commit()  # Commits all changes
    return {"Response": "Match succefully created."}


@app.route('/match/<id>', methods=['PUT'])
@jwt_required
def editMatch(id):
    """Create a match via query string parameters."""
    homeTeam = request.args.get('homeTeam')
    awayTeam = request.args.get('awayTeam')
    date = request.args.get('date')
    match = Match.query.filter_by(matchID=id).first()
    if match is not None:
        match.homeTeam = homeTeam
        match.awayTeam = awayTeam
        match.date = date
        db.session.commit()
        return match_schema.dump(match)
    else:
        return {"Response": "Match not found."}


@app.route('/match/<id>', methods=['DELETE'])
@jwt_required
def deleteMatch(id):
    match = Match.query.filter_by(matchID=id).first()
    if match is not None:
        db.session.delete(match)
        db.session.commit()
        return {"Response": "Match deleted."}
    else:
        return {"Response": "Match not found."}


@app.route('/match/', methods=['DELETE'])
@jwt_required
def deleteMatches():
    matches = Match.query.all()
    if matches is not None:
        for match in matches:
            db.session.delete(match)
        db.session.commit()
        return {"Response": "Matches deleted."}
    else:
        return {"Response": "No matches to delete."}


@app.route('/match', methods=['GET'])
def getMatches():
    matches = Match.query.order_by(Match.matchID).all()
    if matches is not None:
        return matches_schema.dumps(matches)
    else:
        return {"Response": "No matches."}


@app.route('/match/<id>/odds', methods=['GET'])
def getOdds(id):
    odds = RealOdd.query.filter_by(matchID=id).all()
    if odds is not None:
        return odds_schema.dumps(odds)
    else:
        return {"Response": "No odds for this match."}


@app.route('/match/<id>/insertOdd', methods=['POST'])
@jwt_required
def newOdd(id):
    homeOdd = request.args.get('homeOdd')
    drawOdd = request.args.get('drawOdd')
    awayOdd = request.args.get('awayOdd')
    volume = request.args.get('volume')
    new_odd = RealOdd(
        matchID=id,
        homeOdd=homeOdd,
        drawOdd=drawOdd,
        awayOdd=awayOdd,
        volume=volume
    )  # Create an instance of the Odd class
    db.session.add(new_odd)  # Adds new Odd record to database
    db.session.commit()  # Commits all changes
    return {"Response": "Odd succefully created."}


@app.route('/insertOddPredictions', methods=['POST'])
@jwt_required
def insertPredictionOdds():
    oddID = request.args.get('oddID') or request.form.get('oddID')
    matchID = request.args.get('matchID') or request.form.get('matchID')
    oddType = request.args.get('oddType') or request.form.get('oddType')
    openOdd = request.args.get('openOdd') or request.form.get('openOdd')
    highestOdd = request.args.get(
        'highestOdd') or request.form.get('highestOdd')
    lowestOdd = request.args.get('lowestOdd') or request.form.get('lowestOdd')
    actualOdd = request.args.get('actualOdd') or request.form.get('actualOdd')
    volumeOdd = request.args.get('volumeOdd') or request.form.get('volumeOdd')
    MACD_12_26_9 = request.args.get(
        'MACD_12_26_9') or request.form.get('MACD_12_26_9')
    MACDH_12_26_9 = request.args.get(
        'MACDH_12_26_9') or request.form.get('MACDH_12_26_9')
    MACDS_12_26_9 = request.args.get(
        'MACDS_12_26_9') or request.form.get('MACDS_12_26_9')
    SMA_10 = request.args.get('SMA_10') or request.form.get('SMA_10')
    BBL_5 = request.args.get('BBL_5') or request.form.get('BBL_5')
    BBM_5 = request.args.get('BBM_5') or request.form.get('BBM_5')
    BBU_5 = request.args.get('BBU_5') or request.form.get('BBU_5')
    HMA_10 = request.args.get('HMA_10') or request.form.get('HMA_10')
    OBV = request.args.get('OBV') or request.form.get('OBV')
    AROOND_14 = request.args.get('AROOND_14') or request.form.get('AROOND_14')
    AROONU_14 = request.args.get('AROONU_14') or request.form.get('AROONU_14')
    AROONOSC_14 = request.args.get(
        'AROONOSC_14') or request.form.get('AROONOSC_14')
    predict = request.args.get('predict') or request.form.get('predict')

    new_predictedOdd = PredictedOdd(
        oddID=oddID,
        matchID=matchID,
        oddType=oddType,
        openOdd=openOdd,
        highestOdd=highestOdd,
        lowestOdd=lowestOdd,
        actualOdd=actualOdd,
        volumeOdd=volumeOdd,
        MACD_12_26_9=MACD_12_26_9,
        MACDH_12_26_9=MACDH_12_26_9,
        MACDS_12_26_9=MACDS_12_26_9,
        SMA_10=SMA_10,
        BBL_5=BBL_5,
        BBM_5=BBM_5,
        BBU_5=BBU_5,
        HMA_10=HMA_10,
        OBV=OBV,
        AROOND_14=AROOND_14,
        AROONU_14=AROONU_14,
        AROONOSC_14=AROONOSC_14,
        prediction=predict,
    )
    db.session.add(new_predictedOdd)
    db.session.commit()
    return {"Response": "Predicted Odd succefully created."}


@app.route('/deleteAllOddPredictions', methods=['DELETE'])
@jwt_required
def deleteAllPredictedOdds():
    odds = PredictedOdd.query.all()
    if odds is not None:
        for odd in odds:
            db.session.delete(odd)
        db.session.commit()
        return {"Response": "Predicted Odds deleted."}
    else:
        return {"Response": "No predicted odds to delete."}


@app.route('/deleteOddPredictions/<id>', methods=['DELETE'])
@jwt_required
def deletePredictedOdds(id):
    odds = PredictedOdd.query.filter_by(matchID=id).all()
    if odds is not None:
        for odd in odds:
            db.session.delete(odd)
        db.session.commit()
        return {"Response": "Predicted Odds deleted."}
    else:
        return {"Response": "No predicted odds to delete."}


@app.route('/match/<id>/oddPredictions', methods=['GET'])
def getOddPrediction(id):
    odds = PredictedOdd.query.filter_by(matchID=id).all()
    if odds is not None and len(odds) != 0:
        return predicted_odds_schema.dumps(odds)
    else:
        response = {"Response": "No predicted odds yet for this match."}
        return response, 404


@app.route('/generatePredictions', methods=['POST'])
@jwt_required
def generate():
    # generatePredictions()
    return {"Response": "TODO"}


@app.route('/getLastOddGeneratedDate', methods=['GET'])
def getLastGeneratedOdd():
    odd = PredictedOdd.query.order_by(
        PredictedOdd.taOddID.desc()).first()
    if odd is not None:
        return predicted_odd_schema.dump(odd)
    else:
        return {"Response": "No predicted odds yet."}


@app.route('/getLastOddDate', methods=['GET'])
def getLastOdd():
    odd = RealOdd.query.order_by(RealOdd.oddID.desc()).first()
    if odd is not None:
        return odd_schema.dump(odd)
    else:
        return {"Response": "No odds yet."}
