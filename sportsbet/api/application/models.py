"""Data models."""
from . import db
import datetime
from passlib.hash import pbkdf2_sha256 as sha256


class User(db.Model):
    __tablename__ = 'flask_users'
    userID = db.Column(
        db.Integer,
        primary_key=True
    )
    username = db.Column(
        db.String(64),
        index=False,
        unique=True,
        nullable=False
    )
    password = db.Column(
        db.String(64),
        index=False,
        unique=False,
        nullable=False
    )

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


class Match(db.Model):
    __tablename__ = 'flask_matches'
    matchID = db.Column(
        db.Integer,
        primary_key=True
    )

    homeTeam = db.Column(
        db.String(64),
        index=False,
        unique=False,
        nullable=False
    )

    awayTeam = db.Column(
        db.String(64),
        index=False,
        unique=False,
        nullable=False
    )

    date = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False
    )

    matchClosed = db.Column(
        db.Boolean,
        index=False,
        unique=False,
        nullable=False,
        default=False
    )


class RealOdd(db.Model):
    __tablename__ = 'flask_realOdds'
    oddID = db.Column(
        db.Integer,
        primary_key=True
    )
    matchID = db.Column(
        db.Integer,
        db.ForeignKey('flask_matches.matchID')
    )
    matchID = db.Column(db.Integer, db.ForeignKey('flask_matches.matchID'))
    match = db.relationship("Match", backref=db.backref("flask_matches"))
    homeOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    drawOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    awayOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    volume = db.Column(
        db.Integer,
        index=False,
        unique=False,
        nullable=False
    )
    capturedAt = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False,
        default=datetime.datetime.now()
    )


class PredictedOdd(db.Model):
    __tablename__ = 'flask_predOdds'
    taOddID = db.Column(
        db.Integer,
        primary_key=True
    )
    oddID = db.Column(
        db.Integer,
        db.ForeignKey('flask_realOdds.oddID')
    )
    matchID = db.Column(
        db.Integer,
        db.ForeignKey('flask_matches.matchID')
    )
    oddType = db.Column(
        db.String(32),
        index=False,
        unique=False,
        nullable=False
    )
    openOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    highestOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    lowestOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    actualOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    volumeOdd = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=False
    )
    MACD_12_26_9 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    MACDH_12_26_9 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    MACDS_12_26_9 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    SMA_10 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    BBL_5 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    BBM_5 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    BBU_5 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    HMA_10 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    OBV = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    AROOND_14 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    AROONU_14 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    AROONOSC_14 = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    prediction = db.Column(
        db.Float,
        index=False,
        unique=False,
        nullable=True
    )
    generatedAt = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False,
        default=datetime.datetime.now()
    )
