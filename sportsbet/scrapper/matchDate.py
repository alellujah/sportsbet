from datetime import datetime

months = {
    "01": "Janeiro",
    "02": "Fevereiro",
    "03": "Março",
    "04": "Abril",
    "05": "Maio",
    "06": "Junho",
    "07": "Julho",
    "08": "Agosto",
    "09": "Setembro",
    "10": "Outubro",
    "11": "Novembro",
    "12": "Dezembro"
}


def getDayAndYear(string):
    return [str(s) for s in string.split() if s.isdigit()]


def normalizeMonth(string):
    return string.split()[-2].capitalize()


def getMonthNumber(string):
    if (len(string.replace('.', '')) == 3):
        return ([month for (month, value) in months.items()
                 if value[0:3] == string.replace('.', '')][0])
    else:
        return ([month for (month, value) in months.items()
                 if value == string][0])


def getMatchDate(date, hour):
    dayAndYear = getDayAndYear(date)
    monthString = normalizeMonth(date)
    monthNumber = getMonthNumber(monthString)

    date = [dayAndYear[0], str(monthNumber), dayAndYear[1]]
    separator = "-"
    formattedDate = separator.join(date)
    datetime_object = datetime.strptime(formattedDate + ' ' + hour, '%d-%m-%Y %H:%M')

    return datetime_object
