# insert to mysql, change in the future to go to rest API
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import datetime

HOST = 'sportsbet-mysql'
# HOST = 'localhost'
DATABASE = 'sportsbet'
USER = 'root'
PASSWORD = 'ola'


def connectToDB():
    return mysql.connector.connect(
        host=HOST,
        database=DATABASE,
        user=USER,
        password=PASSWORD)

# rest api calls
# import requests

# URL = 'http://localhost:5000'

# def inserMatch(date, homeTeam, awayTeam):
#     matchURL = URL + '/match'
#     match = {'homeTeam': homeTeam,
#             'awayTeam': awayTeam,
#             'date': date }
#     post = requests.post(matchURL, data = match)
#     print(post.text)

# def insertOdd(matchID, homeOdd, drawOdd, awayOdd, volume):
#     oddURL = URL + '/match/' + matchID
#     odd = {'homeOdd': homeOdd,
#             'drawOdd': drawOdd,
#             'awayOdd': awayOdd,
#             'volume': volume}
#     post = requests.post(oddURL, data = odd)
#     print(post.text)


def insertData(date, homeTeam, awayTeam, homeOdd, drawOdd, awayOdd, volume):
    capturedAt = datetime.datetime.now()
    params = (date, homeTeam, awayTeam, homeOdd,
              drawOdd, awayOdd, volume, capturedAt)
    try:
        connection = connectToDB()
        mySql_insert_query = """
            INSERT INTO flask_matches (date, homeTeam, awayTeam, matchClosed)
            VALUES(%s, %s, %s, false);
            INSERT INTO flask_realOdds (matchID, homeOdd, drawOdd, awayOdd, volume, capturedAt) 
            VALUES(LAST_INSERT_ID(),%s, %s, %s, %s, %s);
        """

        cursor = connection.cursor()
        for result in cursor.execute(mySql_insert_query, params, multi=True):
            pass
        print("New match succefully inserted")
        connection.commit()
        cursor.close()

    except mysql.connector.Error as error:
        print("Failed to insert record into matches table {}".format(error))

    finally:
        if (connection.is_connected()):
            connection.close()
            print("MySQL connection is closed")


def checkIfExists(date, homeTeam, awayTeam):
    params = (date, homeTeam, awayTeam)
    try:
        connection = connectToDB()
        mySql_insert_query = """SELECT * FROM flask_matches
                                WHERE date = %s AND homeTeam = %s AND awayTeam = %s"""

        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, params)
        result = cursor.fetchone()
        cursor.close()
        if result != None:
            return True
        else:
            return False

    except mysql.connector.Error as error:
        print("Failed to fecth record from matches table {}".format(error))

    finally:
        if (connection.is_connected()):
            connection.close()


def getMatchID(date, homeTeam, awayTeam):
    params = (date, homeTeam, awayTeam)
    try:        
        connection = connectToDB()
        mySql_insert_query = """SELECT * FROM flask_matches
                                WHERE DATE(date) = DATE(%s) AND homeTeam = %s AND awayTeam = %s LIMIT 1"""

        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, params)
        result = cursor.fetchone()
        cursor.close()
        return result[0]

    except mysql.connector.Error as error:
        print("Failed to fecth id from matches table {}".format(error))

    finally:
        if (connection.is_connected()):
            connection.close()


def updateOdds(matchID, homeOdd, drawOdd, awayOdd, volume):
    capturedAt = datetime.datetime.now()
    params = (matchID, homeOdd, drawOdd, awayOdd, volume, capturedAt)
    try:
        connection = connectToDB()
        mySql_insert_query = """INSERT INTO flask_realOdds (matchID, homeOdd, drawOdd, awayOdd, volume, capturedAt) 
            VALUES(%s,%s, %s, %s, %s, %s);"""

        cursor = connection.cursor()
        cursor.execute(mySql_insert_query, params)
        connection.commit()
        cursor.close()
        print("Odds succefully updated")

    except mysql.connector.Error as error:
        print("Failed to fecth record from realOdds table {}".format(error))

    finally:
        if (connection.is_connected()):
            connection.close()
