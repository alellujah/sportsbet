# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from db import insertData, checkIfExists, getMatchID, updateOdds
from matchDate import getMatchDate
import os

options = webdriver.ChromeOptions()
# options.add_argument('--incognito') # uncomment to open an actual browser
options.add_argument('--headless')
# overcome limited resource problems on container
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--no-sandbox')  # Bypass OS security model

# change path as needed, default is docker container
path_to_chromedriver = '/usr/bin/chromedriver'
# path_to_chromedriver = './chromedriver/83.0/chromedriver_mac'  # change path as needed, default is docker container
driver = webdriver.Chrome(
    executable_path=path_to_chromedriver, options=options)
url = 'https://www.betclic.pt/futebol-s1/portugal-primeira-liga-c32'
driver.get(url)


def waitForLoad(inputXPath):
    Wait = WebDriverWait(driver, 10)
    Wait.until(EC.presence_of_element_located((By.XPATH, inputXPath)))

# link = driver.find_element_by_xpath("//*[@id=' block-link-32']")  #selecionar liga - 32 é PT
# link.click()


# espera que a pagina tenha feito load, tou a usar o breadcrumb como sinal que fez load
waitForLoad(
    "/html/body/app-desktop/div[1]/div/app-content-scroller/div/app-competition/app-competition-header/div/div/bcdk-breadcrumb/div/ul/bcdk-breadcrumb-item[2]/span/li")

driver.execute_script("window.stop();")  # parar o refresh da pagina

matches_by_date = driver.find_elements_by_css_selector(
    "app-sport-event-details > div")


class toSaveonDb:
    def __init__(self, date, homeTeam, awayTeam, homeOdd, drawOdd, awayOdd, volume):
        self.date = date
        self.homeTeam = homeTeam
        self.awayTeam = awayTeam
        self.homeOdd = float(homeOdd.replace(',', '.'))
        self.drawOdd = float(drawOdd.replace(',', '.'))
        self.awayOdd = float(awayOdd.replace(',', '.'))
        self.volume = volume

    def __str__(self):
        return 'homeTeam={0}, awayTeam={1}'.format(self.homeTeam, self.awayTeam)


saveOnDb = []

for match in matches_by_date:
    rawDate = match.find_element_by_css_selector(".eventDate").text
    boxes = match.find_elements_by_css_selector(".betBox_info")
    for box in boxes:
        hour = box.find_element_by_xpath(
            ".//*[starts-with(@class, 'betBox_matchDateInfo')]").text
        match = box.find_element_by_xpath(
            ".//*[starts-with(@class, 'betBox_matchName')]").text
        volume = box.find_element_by_css_selector(
            '.betBox_matchBetsNumber').text
        homeTeam = match.split('-')[0].strip()
        awayTeam = match.split('-')[1].strip()
        date = getMatchDate(rawDate, hour)
        odds = box.find_elements_by_xpath(
            ".//*[starts-with(@class, 'oddValue')]")
        counter = 0  # why?
        oddArray = []
        for odd in odds:
            if counter == 0:
                oddArray.append(odd.text)
            elif counter == 1:
                oddArray.append(odd.text)
            elif counter == 2:
                oddArray.append(odd.text)
            counter += 1
        if checkIfExists(date, homeTeam, awayTeam):
            # update odds
            matchID = getMatchID(date, homeTeam, awayTeam)
            updateOdds(matchID, float(oddArray[0].replace(',', '.')), float(
                oddArray[1].replace(',', '.')), float(oddArray[2].replace(',', '.')), volume)
        else:
            # create new
            saveOnDb.append(toSaveonDb(date, homeTeam, awayTeam,
                                       oddArray[0], oddArray[1], oddArray[2], volume))
driver.quit()


for i in saveOnDb:
    insertData(i.date, i.homeTeam, i.awayTeam,
               i.homeOdd, i.drawOdd, i.awayOdd, i.volume)
