<html>

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.css">
    <style>
        /* @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap'); */
        body {
            font-family: "Roboto";
            padding: 48px;
            background-color: #333333;
            color: white;
        }

        h1 {
            font-size: 48px;
            text-transform: capitalize;
        }

        #wrapper {
            display: flex;
            flex-direction: row;
        }

        #wrapper>div:first-child {
            width: 50%;
            padding-right: 24px;
            display: flex;
            flex-direction: column;
        }

        #wrapper>div:last-child {
            width: 50%;
        }

        #wrapper>div:first-child>div {
            padding: 24px 16px;
            box-shadow: 1px 1px 8px #212121;
            border-radius: 8px;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        #wrapper>div:first-child>div>button {
            border: 0;
            background: transparent;
            color: #ffdd00;
            font-weight: bold;
            font-size: 16px;
            cursor: pointer;
        }

        #wrapper>div:first-child>div>button:hover {
            color: #ffc800;
        }

        form {
            margin-left: 24px;
            display: flex;
            flex-direction: column;
        }

        form>input {
            padding: 16px;
            margin-bottom: 16px;
            width: 100%;
            border-radius: 4px;
            border: 0;
            box-shadow: 1px 1px 8px #2323
        }

        form>button {
            padding: 16px;
            background-color: #ffdd00;
            color: #333333;
            border: 0;
            border-radius: 4px;
            font-weight: bold;
        }

        form>button:hover {
            cursor: pointer;
            background-color: #ffc800;
        }

        #content {
            width: 85%;
            padding-left: 32px;
        }

        #sidenav {
            width: 15%;
            border-right: 1px solid #2b2b2b;
        }

        #sidenav a {
            color: rgba(255, 255, 255, 0.5);
            padding-left: 0;
            margin-top: 24px;
            font-size: 1.3rem;
            width: 100%;
            text-align: left;
        }

        a.button.button-clear.active {
            color: rgba(255, 255, 255, 1) !important;
            transition: all ease .2s;
        }

        .main-wrapper {
            display: flex;
        }
    </style>
</head>
<header>
    <title>SportsBet</title>
</header>

<body>