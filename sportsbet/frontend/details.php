<?php include_once("./inc/header.php") ?>
<div style="display:flex; align-items:center;justify-content:space-between">
    <h2><?= $_GET['matchID'] . ' - ' .  $_GET['homeTeam'] . ' vs ' . $_GET['awayTeam'] .  ' - ' . $_GET['date']  ?> </h2>
    <a class="button button-outline" href="index.php">Voltar ao inicio</a>
</div>

<canvas id="line-chart" width="800" height="450"></canvas>

<h3>Captured Odds (Total captured: <span id="totalCaptured"></span>)</h3>
<table id="game">
    <thead>
        <tr>
            <th>Odd ID</th>
            <th>Home Odd</th>
            <th>Away Odd</th>
            <th>Draw Odd</th>
            <th>Volume</th>
            <th>Captured at</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <!--the one that matters-->
</table>
</div>
<div id="generatedOdds">
    <canvas id="generated" width="800" height="450"></canvas>
    <h3>Generated Predictions (Total generated: <span id="totalGenerated"></span>)</h3>
    <div style="display:flex; align-items: center; justify-content:space-between">
        <h4> <b>Home Odd</b> <span style="font-size: 0.6em">( Right Predictions: <b><span id="homeRight"></span></b> Wrong predictions: <b><span id="homeWrong"></b>)</span></span>
            <br> Actual: <span id="homeOddActual"></span> vs Prediction: <span id="homeOddPrediction"></span> <br>
            Trend is to: <span id="homeOddTrend"></span></h4>
        <h4> <b>Away Odd</b> <span style="font-size: 0.6em">( Right Predictions: <b><span id="awayRight"></span></b> Wrong predictions: <b><span id="awayWrong"></b>)</span></span>
            <br> Actual: <span id="awayOddActual"></span> vs Prediction: <span id="awayOddPrediction"></span> <br>
            Trend is to: <span id="awayOddTrend"></span></h4>
        <h4> <b>Draw Odd</b> <span style="font-size: 0.6em">( Right Predictions: <b><span id="drawRight"></span></b> Wrong predictions: <b><span id="drawWrong"></b>)</span></span>
            <br> Actual: <span id="drawOddActual"></span> vs Prediction: <span id="drawOddPrediction"></span> <br>
            Trend is to: <span id="drawOddTrend"></span></h4>
    </div>
    <div id="container" style="overflow-x: scroll">
        <table id="gamePred">
            <thead>
                <tr>
                    <th>Odd ID</th>
                    <th>Odd Type</th>
                    <th>TAOdd ID</th>
                    <th>Match ID</th>
                    <th>Open Odd</th>
                    <th>Actual Odd</th>
                    <th>Highest</th>
                    <th>Lowest</th>
                    <th>Volume</th>
                    <th>Prediction</th>
                    <th>AROOND_14</th>
                    <th>AROONOSC_14</th>
                    <th>AROONU_14</th>
                    <th>BBL_5</th>
                    <th>BBM_5</th>
                    <th>BBU_5</th>
                    <th>HMA_10</th>
                    <th>MACDH_12_26_9</th>
                    <th>MACDS_12_26_9</th>
                    <th>MACD_12_26_9</th>
                    <th>OBV</th>
                    <th>SMA_10</th>
                    <th>Generated at</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <!--the one that matters-->
        </table>
    </div>
</div>
</body>
<script>
    const labels = [];
    const away = [];
    const draw = [];
    const home = [];
    const gameTable = document.querySelector('#game');
    const gamePred = document.querySelector('#gamePred');
    const URL = 'http://localhost:5000';
    const urlParams = new URLSearchParams(window.location.search);
    const matchID = urlParams.get('matchID');
    // show the captured odds                
    const oddsURL = `/match/${matchID}/odds`;
    fetch(URL + oddsURL).then(function(response) {
        response.json().then(data => {
            console.log('real odds: ', data)
            data.forEach(odd => {
                labels.push(odd.capturedAt);
                away.push(odd.awayOdd);
                home.push(odd.homeOdd);
                draw.push(odd.drawOdd);
                const tr = document.createElement("tr");
                // Insert a row in the table at the last row
                const newRow = gameTable.insertRow();

                // Insert a cell in the row at index 0
                const oddID = newRow.insertCell(0);
                const tdHomeOdd = newRow.insertCell(1);
                const tdAwayOdd = newRow.insertCell(2);
                const tdDrawOdd = newRow.insertCell(3);
                const tdVolume = newRow.insertCell(4);
                const tdCapturedAt = newRow.insertCell(5);

                // Append a text node to the cell
                const id = document.createTextNode(odd.oddID);
                const homeOdd = document.createTextNode(odd.homeOdd);
                const awayOdd = document.createTextNode(odd.awayOdd);
                const drawOdd = document.createTextNode(odd.drawOdd);
                const volume = document.createTextNode(odd.volume);
                const capturedAt = document.createTextNode(odd.capturedAt);

                oddID.appendChild(id);
                tdHomeOdd.appendChild(homeOdd);
                tdAwayOdd.appendChild(awayOdd);
                tdDrawOdd.appendChild(drawOdd);
                tdVolume.appendChild(volume);
                tdCapturedAt.appendChild(capturedAt);
            })
        }).then(() => {
            //after getting values from api 
            // build chart 
            new Chart(document.getElementById("line-chart"), {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        data: away,
                        label: "Away Odd",
                        borderColor: "#3e95cd",
                        fill: false
                    }, {
                        data: home,
                        label: "Home Odd",
                        borderColor: "#8e5ea2",
                        fill: false
                    }, {
                        data: draw,
                        label: "Draw Odd",
                        borderColor: "#3cba9f",
                        fill: false
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Odds fluctuation'
                    }
                }
            });
            //end of chart
        })
    })
    const predictedOddsURL = `/match/${matchID}/oddPredictions`;
    const genLabels = [];
    const oddsHome = [];
    const oddsAway = [];
    const oddsDraw = [];
    const predictionsHome = [];
    const predictionsAway = [];
    const predictionsDraw = [];
    let totalRightHome = 0;
    let totalWrongHome = 0;
    let totalRightAway = 0;
    let totalWrongAway = 0;
    let totalRightDraw = 0;
    let totalWrongDraw = 0;
    fetch(URL + predictedOddsURL).then(function(response) {
        response.json().then(data => {
            console.log('predicted odds: ', data)
            if (data.length === 0) {
                document.querySelector('#generatedOdds').remove()
                const div = document.createElement('div');
                const text = document.createTextNode('No generated data for this match, might be because we dont have sufficient data yet.')
                document.body.append(div.appendChild(text))
                console.log('No generated data for this match')
            }
            data.forEach(predOdd => {
                genLabels.push(predOdd.generatedAt);
                predOdd.oddType === 'home' && oddsHome.push(predOdd.actualOdd);
                predOdd.oddType === 'home' && predictionsHome.push(predOdd.prediction);

                predOdd.oddType === 'away' && oddsAway.push(predOdd.actualOdd);
                predOdd.oddType === 'away' && predictionsAway.push(predOdd.prediction);

                predOdd.oddType === 'draw' && oddsDraw.push(predOdd.actualOdd);
                predOdd.oddType === 'draw' && predictionsDraw.push(predOdd.prediction);

                const tr = document.createElement("tr");
                // Insert a row in the table at the last row
                const newRow = gamePred.insertRow();

                // Insert a cell in the row at index 0
                const tdOddID = newRow.insertCell(0);
                const tdOddType = newRow.insertCell(1);
                const tdTAOddType = newRow.insertCell(2);
                const tdmatchID = newRow.insertCell(3);
                const tdOpenOdd = newRow.insertCell(4);
                const tdActualOdd = newRow.insertCell(5);
                const tdHighestOdd = newRow.insertCell(6);
                const tdLowestOdd = newRow.insertCell(7);
                const tdVolume = newRow.insertCell(8);
                const tdPrediction = newRow.insertCell(9);
                const tdAROOND_14 = newRow.insertCell(10);
                const tdAROONOSC_14 = newRow.insertCell(11);
                const tdAROONU_14 = newRow.insertCell(12);
                const tdBBL_5 = newRow.insertCell(13);
                const tdBBM_5 = newRow.insertCell(14);
                const tdBBU_5 = newRow.insertCell(15);
                const tdHMA_10 = newRow.insertCell(16);
                const tdMACDH_12_26_9 = newRow.insertCell(17);
                const tdMACDS_12_26_9 = newRow.insertCell(18);
                const tdMACD_12_26_9 = newRow.insertCell(19);
                const tdOBV = newRow.insertCell(20);
                const tdSMA_10 = newRow.insertCell(21);
                const tdGeneratedAt = newRow.insertCell(22);

                // Append a text node to the cell
                const TAID = document.createTextNode(predOdd.taOddID);
                const oddID = document.createTextNode(predOdd.oddID);
                const matchID = document.createTextNode(predOdd.matchID);
                const oddType = document.createTextNode(predOdd.oddType);
                const openOdd = document.createTextNode(predOdd.openOdd);
                const prediction = document.createTextNode(predOdd.prediction);
                const volume = document.createTextNode(predOdd.volumeOdd);
                const lowest = document.createTextNode(predOdd.lowestOdd);
                const highest = document.createTextNode(predOdd.highestOdd);
                const actual = document.createTextNode(predOdd.actualOdd);
                const generatedAt = document.createTextNode(predOdd.generatedAt);
                const SMA_10 = document.createTextNode(predOdd.SMA_10);
                const OBV = document.createTextNode(predOdd.OBV);
                const MACD_12_26_9 = document.createTextNode(predOdd.MACD_12_26_9);
                const MACDS_12_26_9 = document.createTextNode(predOdd.MACDS_12_26_9);
                const MACDH_12_26_9 = document.createTextNode(predOdd.MACDH_12_26_9);
                const HMA_10 = document.createTextNode(predOdd.HMA_10);
                const BBU_5 = document.createTextNode(predOdd.BBU_5);
                const BBM_5 = document.createTextNode(predOdd.BBM_5);
                const BBL_5 = document.createTextNode(predOdd.BBL_5);
                const AROONU_14 = document.createTextNode(predOdd.AROONU_14);
                const AROONOSC_14 = document.createTextNode(predOdd.AROONOSC_14);
                const AROOND_14 = document.createTextNode(predOdd.AROOND_14);

                tdOddID.appendChild(oddID);
                tdOddType.appendChild(oddType);
                tdTAOddType.appendChild(TAID);
                tdmatchID.appendChild(matchID);
                tdOpenOdd.appendChild(openOdd);
                tdActualOdd.appendChild(actual);
                tdHighestOdd.appendChild(highest);
                tdLowestOdd.appendChild(lowest);
                tdVolume.appendChild(volume);
                tdPrediction.appendChild(prediction);
                tdAROOND_14.appendChild(AROOND_14);
                tdAROONOSC_14.appendChild(AROONOSC_14);
                tdAROONU_14.appendChild(AROONU_14);
                tdBBL_5.appendChild(BBL_5);
                tdBBM_5.appendChild(BBM_5);
                tdBBU_5.appendChild(BBU_5);
                tdHMA_10.appendChild(HMA_10);
                tdMACDH_12_26_9.appendChild(MACDH_12_26_9);
                tdMACDS_12_26_9.appendChild(MACDS_12_26_9);
                tdMACD_12_26_9.appendChild(MACD_12_26_9);
                tdOBV.appendChild(OBV);
                tdSMA_10.appendChild(SMA_10);
                tdGeneratedAt.appendChild(generatedAt);
            })
        }).then(() => {
            // Check if it got prediction right!
            for (let index = 0; index < oddsHome.length - 1; index++) {
                if (index + 1 < oddsHome.length - 1) {
                    predictionsHome[index].toFixed(2) == oddsHome[index + 1] ? totalRightHome++ : totalWrongHome++;
                    predictionsAway[index].toFixed(2) == oddsAway[index + 1] ? totalRightAway++ : totalWrongAway++;
                    predictionsDraw[index].toFixed(2) == oddsDraw[index + 1] ? totalRightDraw++ : totalWrongDraw++;
                }
            }
            const homeRight = document.querySelector('#homeRight');
            const homeRightText = document.createTextNode(totalRightHome);
            homeRight.appendChild(homeRightText);
            const homeWrong = document.querySelector('#homeWrong');
            const homeWrongText = document.createTextNode(totalWrongHome);
            homeWrong.appendChild(homeWrongText);

            const awayRight = document.querySelector('#awayRight');
            const awayRightText = document.createTextNode(totalRightAway);
            awayRight.appendChild(awayRightText);
            const awayWrong = document.querySelector('#awayWrong');
            const awayWrongText = document.createTextNode(totalWrongAway);
            awayWrong.appendChild(awayWrongText);

            const drawRight = document.querySelector('#drawRight');
            const drawRightText = document.createTextNode(totalRightDraw);
            drawRight.appendChild(drawRightText);
            const drawWrong = document.querySelector('#drawWrong');
            const drawWrongText = document.createTextNode(totalWrongDraw);
            drawWrong.appendChild(drawWrongText);

            const totalCaptured = document.querySelector('#totalCaptured');
            const totalCapturedText = document.createTextNode(oddsHome.length + oddsDraw.length + oddsAway.length);
            totalCaptured.appendChild(totalCapturedText);

            const totalGenerated = document.querySelector('#totalGenerated');
            const totalGeneratedText = document.createTextNode(predictionsHome.length + predictionsDraw.length + predictionsAway.length);
            totalGenerated.appendChild(totalGeneratedText);

            const nextHomeOdd = document.querySelector('#homeOddPrediction');
            const actualHomeOdd = document.querySelector('#homeOddActual');
            const trendHomeOdd = document.querySelector('#homeOddTrend');
            const nextHomeOddText = document.createTextNode(predictionsHome[predictionsHome.length - 1].toFixed(2));
            const actualHomeOddText = document.createTextNode(oddsHome[oddsHome.length - 1]);
            const trendHomeOddCount = predictionsHome[predictionsHome.length - 1].toFixed(2) - oddsHome[oddsHome.length - 1];
            let trendHomeOddText = document.createTextNode(
                trendHomeOddCount > 0 ? 'Up' : 'Down'
            );
            if (trendHomeOddCount == 0) {
                trendHomeOddText = document.createTextNode(
                    'Mantain'
                );
            }
            nextHomeOdd.appendChild(nextHomeOddText);
            actualHomeOdd.appendChild(actualHomeOddText);
            trendHomeOdd.appendChild(trendHomeOddText);

            const nextAwayOdd = document.querySelector('#awayOddPrediction');
            const actualAwayOdd = document.querySelector('#awayOddActual');
            const trendAwayOdd = document.querySelector('#awayOddTrend');
            const nextAwayOddText = document.createTextNode(predictionsAway[predictionsAway.length - 1].toFixed(2));
            const actualAwayOddText = document.createTextNode(oddsAway[oddsAway.length - 1]);
            const trendAwayOddCount = predictionsAway[predictionsAway.length - 1].toFixed(2) - oddsAway[oddsAway.length - 1];
            console.log(trendAwayOdd)
            let trendAwayOddText = document.createTextNode(
                trendAwayOddCount > 0 ? 'Up' : 'Down'
            );
            if (trendAwayOddCount == 0) {
                trendAwayOddText = document.createTextNode(
                    'Mantain'
                );
            }
            nextAwayOdd.appendChild(nextAwayOddText);
            actualAwayOdd.appendChild(actualAwayOddText);
            trendAwayOdd.appendChild(trendAwayOddText);

            const nextDrawOdd = document.querySelector('#drawOddPrediction');
            const actualDrawOdd = document.querySelector('#drawOddActual');
            const trendDrawOdd = document.querySelector('#drawOddTrend');
            const nextDrawOddText = document.createTextNode(predictionsDraw[predictionsDraw.length - 1].toFixed(2));
            const actualDrawOddText = document.createTextNode(oddsDraw[oddsDraw.length - 1]);
            const trendDrawOddCount = predictionsDraw[predictionsDraw.length - 1].toFixed(2) - oddsDraw[oddsDraw.length - 1];
            console.log(trendDrawOdd)
            let trendDrawOddText = document.createTextNode(
                trendDrawOddCount > 0 ? 'Up' : 'Down'
            );
            if (trendDrawOddCount == 0) {
                trendDrawOddText = document.createTextNode(
                    'Mantain'
                );
            }
            nextDrawOdd.appendChild(nextDrawOddText);
            actualDrawOdd.appendChild(actualDrawOddText);
            trendDrawOdd.appendChild(trendDrawOddText);

            //after getting values from api 
            // build chart             
            new Chart(document.getElementById("generated"), {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        data: oddsHome,
                        label: "Odd Home",
                        borderColor: "#3e95cd",
                        fill: false
                    }, {
                        data: predictionsHome,
                        label: "Prediction Home",
                        borderColor: "#8e5ea2",
                        fill: false
                    }, {
                        data: oddsAway,
                        label: "Odd Away",
                        borderColor: "#7db6e5",
                        fill: false
                    }, {
                        data: predictionsAway,
                        label: "Prediction Away",
                        borderColor: "#8280a5",
                        fill: false
                    }, {
                        data: oddsDraw,
                        label: "Odd Draw",
                        borderColor: "#fbbc85",
                        fill: false
                    }, {
                        data: predictionsDraw,
                        label: "Prediction Draw",
                        borderColor: "#26716f",
                        fill: false
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Odds real vs prediction'
                    }
                }
            });
            //end of chart
        })
    })
</script>
<?php include_once("./inc/footer.php") ?>