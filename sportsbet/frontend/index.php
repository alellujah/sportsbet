<?php include_once("./inc/header.php") ?>
<div class="main-wrapper">
    <?php include_once('./inc/sidenav.php') ?>
    <!-- <canvas id="myChart" width="200" height="200"></canvas> -->
    <div id="content">
        <h1 style="display:flex;justify-content:space-between; align-items:center;">All games <span style="font-size:1.4rem;color:#c3c3c3">Last fetch: <span id="lastFetch"></span> Last generated predictions: <span id="lastGenerated"></span></span></h1>
        <div id="container">
            <table id="game">
                <thead>
                    <tr>
                        <th>Match ID</th>
                        <th>Home Team</th>
                        <th>Away Team</th>
                        <th>Date and time</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <!--the one that matters-->
            </table>
        </div>
        <div id="wrapper">
            <div id="jogos">
            </div> <!-- Div que vai conter os jogos -->
        </div>
    </div>
</div> <!-- end of main-wrapper -->
</body>
<script>
    const URL = 'http://localhost:5000';

    // Fetch da lista de jogos
    const gameTable = document.querySelector('#game');
    fetch(URL + '/getLastOddGeneratedDate').then(function(response) {
        response.json().then(data => {
            textNode = document.createTextNode(data.generatedAt)
            document.querySelector('#lastGenerated').appendChild(textNode)
            console.log('last gen: ', data)
        })
    })
    fetch(URL + '/getLastOddDate').then(function(response) {
        response.json().then(data => {
            textNode = document.createTextNode(data.capturedAt)
            document.querySelector('#lastFetch').appendChild(textNode)
            console.log('last odd: ', data)
        })
    })
    fetch(URL + '/match').then(function(response) {
            if (response.ok) {
                response.json().then(data => { // data é o ReadableStream transformado em json atraves do metodo .json()
                    console.log(data)
                    data.forEach(match => { // match é o objeto json 
                        const tr = document.createElement("tr");
                        // Insert a row in the table at the last row
                        const newRow = gameTable.insertRow();

                        // Insert a cell in the row at index 0
                        const tdID = newRow.insertCell(0);
                        const tdHomeTeam = newRow.insertCell(1);
                        const tdAwayTeam = newRow.insertCell(2);
                        const tdDate = newRow.insertCell(3);
                        const tdStatus = newRow.insertCell(4);

                        const btn = document.createElement("button");
                        const viewBtn = newRow.insertCell(5);

                        // Append a text node to the cell
                        const id = document.createTextNode(match.matchID);
                        const homeTeam = document.createTextNode(match.homeTeam);
                        const awayTeam = document.createTextNode(match.awayTeam);
                        const date = document.createTextNode(match.date);
                        const status = document.createTextNode(match.matchClosed ? 'Closed' : 'Open');
                        const viewText = document.createTextNode('View details');
                        tdID.appendChild(id);
                        tdHomeTeam.appendChild(homeTeam);
                        tdAwayTeam.appendChild(awayTeam);
                        tdDate.appendChild(date);
                        tdStatus.appendChild(status);

                        btn.innerText = 'View details';
                        viewBtn.appendChild(btn);

                        btn.onclick = function() {
                            window.location.href = `http://localhost:9000/details.php?matchID=${match.matchID}&homeTeam=${match.homeTeam}&awayTeam=${match.awayTeam}&date=${match.date}`;
                        };
                        // const btn = document.createElement("button");
                        // btn.innerText = "Delete";
                        // btn.onclick = function(){
                        //     //delete this match
                        //     fetch(URL + '/match/' + match.matchID , {method: 'delete'}).then(function() {
                        //     location.reload();                            
                        //     })                
                        // };                
                        // div.appendChild(btn);                

                    });
                })
            } else {
                console.log('Network response was not ok.');
            }
        })
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
        });
</script>
<?php include_once("./inc/footer.php") ?>